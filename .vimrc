set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'itchyny/lightline.vim'
Plugin 'francoiscabrol/ranger.vim'
Plugin 'lilydjwg/colorizer'
Plugin 'airblade/vim-gitgutter'
Plugin 'Yggdroot/indentLine'
Plugin 'rhysd/vim-gfm-syntax'
Plugin 'doums/darcula'
Plugin 'jiangmiao/auto-pairs'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

syntax enable
set mouse=v
set noshowmode
set laststatus=2
set nu rnu
set cursorline
set hlsearch
hi LineNr term=bold cterm=NONE ctermfg=DarkGray ctermbg=NONE gui=NONE guifg=NONE guibg=NONE
hi CursorLineNr term=NONE cterm=NONE ctermfg=Yellow ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE
hi CursorLine term=NONE cterm=NONE
hi Search cterm=NONE ctermfg=black ctermbg=red

let g:lightline = {
      \ 'colorscheme': 'darcula',
      \ }

" shortcut
function! NumberToggle()
  if(&rnu == 1)
    set nonu nornu
  else
    set nu rnu
  endif
endfunc

nnoremap <C-l> :call NumberToggle()<cr>


let vim_markdown_preview_browser_hotkey='<C-m>'
let vim_markdown_preview_browser='qutebrowser'

highlight clear SignColumn

"ranger vim plugin
let g:ranger_map_keys = 0
map <leader>f :RangerCurrentDirectoryNewTab<CR>

"hi Normal guibg=NONE ctermbg=NONE /kalo pake colorscheme ini harus dibawahnya

let g:indentLine_char = '┊'
let g:indentLine_color_term = 240





ab qwqw gacor ini asli
ab wer LOOOOOOOOOOOOL
